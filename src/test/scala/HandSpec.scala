package blackjack

import org.scalatest._

class HandSpec extends FunSpec {
  describe("A Hand") {
    it("should have value 13 with ace") {
      val hand = new Hand
      hand.addCard(new Card("Heart","Ace"))
      hand.addCard(new Card("Spade","2"))
      assert(hand.value === 13)
      assert(hand.hasAce == true)
   }

   it("should have value 16 with no aces") {
     val hand = new Hand
     hand.addCard(new Card("Diamond", "King"))
     hand.addCard(new Card("Club", "6"))
     assert(hand.value === 16)
       assert(hand.hasAce == false)
   }
}
}

   
