package blackjack

import org.scalatest._

class CardSpec extends FunSpec {
  describe("A Card") {
    it("should valuate Queen of Heart correctly") {
      val card = new Card("Heart", "Queen")
      assert(card.value == 10)
    }

  }
}
