package blackjack

import org.scalatest._

class PlayerSpec extends FunSpec{
  describe("Player"){
    val player = new Player
    it("should have 400 after betting 100"){
      player.betMoney(100)
      assert(player.money === 400.0)
    }
    it("should have 350 after insurance"){
      player.insure()
      assert(player.money === 350.0)
    }
    it("current bet should be 150 after double down"){
      player.doubleDown
      assert(player.money === 300.0)
    }
  }
}

