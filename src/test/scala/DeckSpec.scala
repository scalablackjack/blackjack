package blackjack

import org.scalatest._

class DeckSpec extends FunSpec {
  describe("A Simple Deck") {

    it("should have 52 cards") {
      val deck = new Deck(1)
      val cards = deck.getOneFullDeck
      assert(cards.size === 52)
    }

    it("should have 380 value") {
      val deck = new Deck(1)
      deck.makeAllDecks
      var expected = 0
      for (i <- 1 to 52) expected += deck.dealCard.value
      val actual = 380
      assert(actual === expected)
    }

    it("should have 50 cards after dealing 2 cards") {
      val deck = new Deck(1)
      deck.makeAllDecks
      deck.dealCard
      deck.dealCard
      assert(deck.cards.size === 50)
    }

    it("should have 0 cards after dealing 52 cards") {
      val deck = new Deck(1)
      deck.makeAllDecks
      for (i <- 0 until 52) deck.dealCard
      assert(deck.cards.size === 0)
    }
}
  describe("A complex Deck"){
    it("should have 156 cards in 3 decks") {
      val deck = new Deck(3)
      deck.makeAllDecks()
      val cards = deck.cards
      assert(cards.size === 156)
}
}
}
