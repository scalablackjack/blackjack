package blackjack
import scala.collection.mutable

class Game(val decks: Int){

  var gameDeck = new Deck(decks)

  val player = new Player()

  var playerStand = false

  val dealerHand = new Hand

  var gameOn = false

  var gameResult = ""

  def playerCards = {
    var currentP = mutable.ListBuffer[Card]()
      player.playerHand.currentCards.foreach(c => currentP += c )
      currentP
  }

  def dealerCards = {
    var currentD = mutable.ListBuffer[Card]()
      dealerHand.currentCards.foreach(c => currentD += c )
      currentD
  }

  def playHit = {
    player.playerHand.addCard(gameDeck.dealCard)
    if(player.playerHand.value >= 21) {getResult; gameOn = false}
  }

  def getCurrentBet: Double = {
    player.currentBet
  }

  def getMoney: Double = {
    player.money
  }

  def playTurn(decision: String) = {
    decision match {
      case "DD" =>  player.doubleDown; playHit
      case "H" => playHit
      case "S" => playerStand = true; getResult; gameOn = false
      case "I" => insure
    }
  }

  def bet(m: Double): Unit ={
    clear
    gameResult = ""
    gameOn = true
    player.betMoney(m)
  }

  /** returning value of visible card to check if we can insure or not */
  def firstDeal: Card = {
    gameDeck.makeAllDecks
    gameDeck.shuffle
    playHit
    playHit
    val visibleCard = gameDeck.dealCard
    dealerHand.addCard(visibleCard)
    dealerHand.addCard(gameDeck.dealCard)
    visibleCard
  }

  /** we return true if game is still on, or false if not*/
  def insure: Boolean = {
    player.insure()
    if(dealerHand.value == 21) {gameResult = "DRAW!"; false}
    else true
  }

  def getResult = {
    if(player.playerHand.value > 21) {gameResult = "You Lost!";player.playerLost } else {
      if(player.playerHand.value == 21) {gameResult = "You Won!";player.playerWon }
      else {
        while (dealerHand.value < 17) {
          dealerHand.addCard(gameDeck.dealCard)
        }
        if (dealerHand.value > 21) {
          gameResult = "You won!"; player.playerWon
        }
        else if (dealerHand.value == player.playerHand.value) {
          gameResult = "DRAW!"
          player.draw
        }
        else if (dealerHand.value > player.playerHand.value) {
          gameResult = "You Lost!"; player.playerLost
        }
        else {
          gameResult = "You won!"; player.playerWon
        }
      }
    }

  }

  def clear = {
    player.playerHand.clear()
    dealerHand.clear()
  }

}
