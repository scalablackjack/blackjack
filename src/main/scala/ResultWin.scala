
package blackjack

import scala.swing._
import scala.swing.event.ButtonClicked
import  java.awt.{Color, Font}

class ResultWin(message : String) extends MainFrame{

title = "End of game"
preferredSize = new Dimension(300, 200)
background = Color.CYAN

val b = new Button {
text = "NEXT DEAL"
}

contents = new BoxPanel(Orientation.Vertical){
contents += new Label ( message){
font = new Font ( "TimesRoman", Font.PLAIN, 30)
}

contents+= b
}

listenTo(b)
reactions += {
case ButtonClicked(b) => {
//val gameWin = new GameWindow(deckBox.item)
//gameWin.visible = true
dispose()
}
}
}
