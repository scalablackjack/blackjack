package blackjack

class Card(val Suite: String, val Rank: String){

  val value = {
    Rank match {
      case "Ace" => 11
      case "King" | "Queen" | "Jack" => 10
      case _ => Rank.toInt
    }
  }

}
