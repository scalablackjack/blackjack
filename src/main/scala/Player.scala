package blackjack

class Player(){

 val playerHand = new Hand
 var money = 500.0
 var currentBet = 0.0


 def betMoney(m : Double){
   if(m<= money){
    currentBet = m
    money = money - m
  }
}

  def doubleDown{
    if (money >= 0.5 * currentBet ) {
       money = money - 0.5 * currentBet
       currentBet = 1.5 * currentBet
    }
  }

  def insure() {
    if(money >= currentBet /2 )
      money = money - currentBet /2
  }

 def playerLost: Unit = {
  currentBet = 0.0
 }

 def playerWon: Unit ={
  if(playerHand.value == 21) money += currentBet * 5 else money += currentBet * 2
  currentBet = 0.0
 }

 def draw: Unit = {
  money += currentBet
  currentBet = 0.0
 }
}
