package blackjack

import scala.swing._
import scala.swing.event.ButtonClicked
import java.awt.{Color,Font}
import javax.swing.ImageIcon
import java.awt.image.BufferedImage
import java.io._

class Start extends MainFrame {
  title = "BlackJack Game"
  preferredSize = new Dimension(560, 300)

  val imgLabel = new Label {
  icon = new ImageIcon ("bj.jpg")
}

  val b = new Button {
text = "START"
minimumSize = new Dimension(100,60)
maximumSize = minimumSize
preferredSize = minimumSize}
val deckBox = new ComboBox(1 to 8){
preferredSize = new Dimension(40,40)
}

val deckPanel = new BorderPanel{
add( imgLabel, BorderPanel.Position.North)
add (new Label{
text = "Choose the number of decks: "
font = new Font("TimesRoman", Font.PLAIN, 30)
}, BorderPanel.Position.Center)
add (deckBox, BorderPanel.Position.South)
}

contents = new BoxPanel(Orientation.Horizontal){
  contents += deckPanel
  contents += b
  }
  listenTo(b)
  reactions += {
  case ButtonClicked(b) => {
  val gameWin = new GameWindow(deckBox.item)
  gameWin.visible = true
  dispose()
  }
  }
}

object GuiSetUp {
  def main(args: Array[String]) {
    val start = new Start
    start.visible = true
  }
}
