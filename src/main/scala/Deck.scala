package blackjack

import scala.util.Random
import scala.math
import scala.collection.mutable

class Deck(val numberOfDecks: Int){

  val suites = Array("Heart", "Diamond", "Club", "Spade")

  val ranks = Array("Ace", "King", "Queen", "Jack", "2", "3",
    "4", "5", "6", "7", "8", "9", "10")

  val cards = mutable.Buffer[Card]()

  def getOneFullDeck: mutable.Buffer[Card] = {
    val fullDeck = mutable.Buffer[Card]()
    suites.foreach{ suite =>
      ranks.foreach { rank =>
        fullDeck += new Card(suite, rank)
      }
    }
    fullDeck
  }

  def makeAllDecks() {
    val fullDeck = getOneFullDeck
    for (i <- 1 to numberOfDecks) cards ++= fullDeck
  }

  def shuffle() {
    val maxIndex = cards.length - 1
    for (k <- maxIndex until 1 by -1){
      val range = k - 1
      val randIndex = math.abs(Random.nextInt) % range
      val temp = cards(k)
      cards(k) = cards(randIndex)
      cards(randIndex) = temp
    }
  }

  def dealCard: Card = {
    cards.remove(0)
  }

}
