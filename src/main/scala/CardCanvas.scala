
package blackjack

import java.awt.Graphics
import java.awt.Font
import javax.swing.JComponent
import java.awt.Color
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.io._


class CardCanvas(c: Card) extends JComponent {
val img = { c.Suite match {
case "Heart" => ImageIO.read(new File("src/main/scala/kier.jpg"))
case "Diamond" => ImageIO.read(new File("src/main/scala/karo.jpg"))
case "Club"=> ImageIO.read(new File("src/main/scala/trefl.jpg"))
case "Spade" => ImageIO.read(new File("src/main/scala/pik.jpg"))
}
}
val r = { c.Rank match {
case "10" => c.Rank
case _ => c.Rank.substring(0,1)
}

}

override def paint(g:Graphics ) {
    g.setColor(Color.RED)
    g.drawRoundRect (10, 10, 90, 150, 25, 25)
    g.setColor(Color.WHITE)
    g.fillRoundRect(10,10,90,150,25,25)
    g.setColor(Color.BLACK)
    g.setFont(new Font("TimesRoman", Font.PLAIN, 20))
    g.drawString(r,80,30)
    g.drawImage(img,20,40,this)
    g.drawString(r,20,150)
  }
}
