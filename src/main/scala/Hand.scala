package blackjack

import scala.math._
import scala.collection.mutable

class Hand(){

var cards = mutable.ListBuffer[Card]()
var hasAce = false
  var value  = 0

def addCard(card: Card) {
  if (card.value == 11)
    hasAce = true

  cards += card
  value += card.value

  }

  def clear() {
    value = 0
   cards.clear()
   hasAce = false
 }

def currentCards() = cards
}
