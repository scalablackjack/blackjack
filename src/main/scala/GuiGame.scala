package blackjack

import scala.swing._
import scala.swing.event.ButtonClicked
import java.awt.{Color, Font}
import javax.swing.Box
import scala.swing.TextField


class GameWindow (decks : Int) extends MainFrame  {

  val game = new Game(decks)

  var dealerCard = null.asInstanceOf[Card]

  title = "BlackJack Game Player"
  preferredSize = new Dimension(960, 640)

  val stand = new Button {
  text = "STAND"
  }

  val hit = new Button {
  text = "HIT"
  }

  val bet = new Button {
  text = "BET"
  }

  val insure = new Button {
  text = "INSURE"
  }

  val doubleDown =  new Button {
  text = "DOUBLEDOWN"
  }

  val moneyArea = new TextField{
    foreground = Color.RED
    background = Color.GREEN
    maximumSize = new Dimension(50,50)
  }

  val betArea = new TextField{
    foreground = Color.GREEN
    background = Color.RED
    maximumSize = new Dimension(50,50)
  }

  val budgetArea = new TextField{
    foreground = Color.GREEN
    background = Color.RED
    maximumSize = new Dimension(50,50)
  }

  val cardsPanel = new BoxPanel(Orientation.Horizontal){
  background = Color.GREEN
  }
   val dealerPanel = new BoxPanel(Orientation.Vertical){
  background = Color.BLUE
  }
  val cPanel = new BoxPanel(Orientation.Vertical){
  contents+= new Label(" YOUR CARDS "){
  font = new Font("TimesRoman", Font.PLAIN, 30)
  }
  contents += cardsPanel
  }
  val dPanel = new BoxPanel(Orientation.Vertical){
  contents+= new Label(" DEALER "){
  font = new Font("TimesRoman", Font.PLAIN, 30)
  }
  contents += dealerPanel
  }

val rightSide = new BoxPanel(Orientation.Horizontal){
background = Color.YELLOW

  contents += dPanel

  contents += new BoxPanel(Orientation.Vertical){
  background = Color.CYAN
    peer.add(Box.createHorizontalStrut(1))
  contents += hit
    peer.add(Box.createHorizontalStrut(1))
  contents += stand
    peer.add(Box.createHorizontalStrut(1))
  contents += insure
    peer.add(Box.createHorizontalStrut(1))
  contents += doubleDown
    peer.add(Box.createHorizontalStrut(1))

  contents += new BoxPanel(Orientation.Horizontal){
    contents+= moneyArea
    contents+= bet
    contents += betArea
    contents += budgetArea
  }
    peer.add(Box.createHorizontalStrut(1))
  }

  contents+= cPanel
  }
  contents = rightSide

  listenTo(hit)
    reactions += {
      case ButtonClicked(`hit`) => if(game.gameOn) {
    game.playTurn("H"); updateState
      }
    }
  listenTo(stand)
    reactions += {
      case ButtonClicked(`stand`) => if(game.gameOn) {game.playTurn("S"); updateState}
    }
  listenTo(doubleDown)
    reactions += {
      case ButtonClicked(`doubleDown`) => if(game.gameOn) {game.playTurn("DD"); updateState}
    }
  listenTo(bet)
    reactions += {
    case ButtonClicked(`bet`) => if(!game.gameOn){
    if (moneyArea.text.toDouble<game.getMoney){
    game.bet(moneyArea.text.toDouble)
    dealerCard = game.firstDeal
    updateState
    }
                             }
  }

  listenTo(insure)
    reactions += {
      case ButtonClicked(`insure`) => if(dealerCard.value==11) game.insure
    }


  def updateState = {
    betArea.text = game.getCurrentBet.toString
    budgetArea.text = game.getMoney.toString

    cardsPanel.contents.clear
    cardsPanel.revalidate
    cardsPanel.repaint
    game.playerCards.foreach( c =>   cardsPanel.contents +=  Component.wrap(new CardCanvas(c)) )
    cardsPanel.revalidate
    cardsPanel.repaint


    dealerPanel.revalidate
      dealerPanel.contents.clear
      dealerPanel.revalidate
     dealerPanel.contents += Component.wrap(new CardCanvas( dealerCard))
      dealerPanel.revalidate

      if (!game.gameOn)  {
        dealerPanel.contents.clear
        game.dealerCards.foreach( c =>   dealerPanel.contents +=  Component.wrap(new CardCanvas(c)) )
        dealerPanel.revalidate
        dealerPanel.repaint
        showResult()
      }
  }

  def showResult(): Unit ={
    var resultWin = new ResultWin(game.gameResult)
    resultWin.visible = true
  }

}
